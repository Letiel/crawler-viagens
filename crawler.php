<!DOCTYPE html>
<html>
	<head>
		<title>Letiel.test</title>
		<meta charset='utf-8' />
		<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<style type="text/css">
			.br{
				display: block;
			}
		</style>
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
		<script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js'></script>
	</head>
	<body>
		<div class="row panel panel-default col-md-10 col-md-offset-1">
			<div class="panel-body">
				<form action='#' method='post'>
					<div class="form-group">
						<input required='required' class="form-control" id='url_cidade' />
					</div>

					<div class="form-group">
						<?php include 'class/bd.php';
								if(!$banco)
									echo "Não conectou no banco de dados!"; ?>
						<select id="cidade" class='form-control'>
							<option value=''>Selecione</option>
							<?php
								$query = mysqli_query($banco, "SELECT * FROM lc_terms JOIN lc_term_taxonomy ON (lc_term_taxonomy.term_id = lc_terms.term_id) WHERE lc_term_taxonomy.parent = 0");
								while($categoria = mysqli_fetch_array($query)){
									echo utf8_encode("<option value='$categoria[term_id]'>$categoria[name]</option>");
								}
							?>
						</select>
					</div>
					<div class="form-group">
						<button id='submit' class="btn btn-default" type='submit'>Adicionar na fila</button>
						<button id='add' class="btn btn-primary" type='button' onclick='rodar()'>Rodar</button>
					</div>
				</form>
				<div>
					<p>
						Fila:
					</p>
					<table class='table table-striped' id='array'></table>
				</div>
			</div>
		</div>
		<div class="row panel panel-default col-md-10 col-md-offset-1">
			<div class="panel-body">
				<div class='alert alert-info' id='aguarde' style='display: none;'>
					<b>Aguarde...</b>
					<img src='/crawler_agro/img/loading.gif' style='width: 170px;' />
				</div>
			</div>
			<table id="resultado"></table>
		</div>

		<audio id="audio">
		    <source src="alarme.mp3" type="audio/mpeg">
		    Seu navegador não possui suporte ao elemento audio
		</audio>

		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

		<script type="text/javascript">

			$("#cidade").select2();


			audio = document.getElementById('audio');

		    function play(){
		        audio.play();
		    }



				array = [];
				print_array();
				$(".retirar").click(function(){
					array.splice($(this).val(), 1);
					$(this).parent().remove();
				});

				function retirar_index(i){
					array.splice(i, 1);
					print_array();
				}

				function rodar(){
					if(array.length == 0)
						exit();
					$("#aguarde").show();
					$.post("crawler_geral.php",
					{
						url_cidade: array[0]['url'],
						cidade: array[0]['categoria'],
					},
					function(result){
		        	$("#resultado").append(result);
		        	$("#aguarde").hide();
							array.splice(0, 1);
							print_array();
							if(array.length > 0)
								rodar();
							else
								play();
				    });
				}

				$('form').submit(function(e){
					item = {'categoria': $("#cidade").val(), 'url': $("#url_cidade").val()};
					// $('#cidade').prop('selectedIndex',0);
					$("#url_cidade").val("");
					array.push(item);
					print_array();
					e.preventDefault();
				});

				function print_array(){
					$("#array").html("");
					for (index = 0; index < array.length; ++index) {
							$("#array").append("<tr><td>Categoria "+array[index]['categoria']+'</td><td>'+array[index]['url']+"</td><td><button class='btn btn-danger retirar' onclick='retirar_index("+index+")'>Retirar</button></td></tr>");
					}
					if(array.length == 0)
						$("#array").html("<tr><td>Nenhum item na fila.</td></tr>");
				}



		</script>
	</body>
</html>
