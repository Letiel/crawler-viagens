<html lang="pt-BR">
  <head>
    <meta charset="utf-8">
    <title>Notifications</title>
   

    <meta charset='utf-8' />
    <!-- <link rel="stylesheet" type="text/css" href="/css/bootstrap.css"> -->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <!-- // <script type="text/javascript" src='/js/jquery-2.1.4.min.js'></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <!-- // <script type="text/javascript" src='/js/bootstrap.min.js'></script> -->
    <script type="text/javascript" src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js'></script>

    
    <!-- Notify CSS -->
    <link href="/bt-notifications/css/bootstrap-notify.css" rel="stylesheet">

    <!-- Custom Styles -->
    <link href="/bt-notifications/css/styles/alert-bangtidy.css" rel="stylesheet">
    <link href="/bt-notifications/css/styles/alert-blackgloss.css" rel="stylesheet">
    
    <style>
      body {
        margin-bottom: 50px;
      }
    </style>
    

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  </head>

  <body>
    <div class="container">
      <div class="row">
        <div class="span3">
          <div class="span12">
            <button class='btn btn-warning show-notification'>Click For A Random Notification</button>
          </div>

          <div class='notifications top-right'></div>
          <div class='notifications bottom-right'></div>
          <div class='notifications top-left'></div>
          <div class='notifications bottom-left'></div>
        </div>
        
      </div>
      <div class="row">
        <div class="span3">
          <div class="span12">&nbsp;</div>
          
          <div class="span3">
            <button class='btn show-bangTidy'>bangTidy</button>
            <button class='btn show-blackgloss'>blackgloss</button>
          </div>
        </div>
      </div>
    </div> <!-- /container -->

    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script type="text/javascript">
      /* ===================================================
       * bootstrap-transition.js v2.3.2
       * http://twbs.github.com/bootstrap/javascript.html#transitions
       * ===================================================
       * Copyright 2013 Twitter, Inc.
       *
       * Licensed under the Apache License, Version 2.0 (the "License");
       * you may not use this file except in compliance with the License.
       * You may obtain a copy of the License at
       *
       * http://www.apache.org/licenses/LICENSE-2.0
       *
       * Unless required by applicable law or agreed to in writing, software
       * distributed under the License is distributed on an "AS IS" BASIS,
       * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       * See the License for the specific language governing permissions and
       * limitations under the License.
       * ========================================================== */


      !function ($) {

        "use strict"; // jshint ;_;


        /* CSS TRANSITION SUPPORT (http://www.modernizr.com/)
         * ======================================================= */

        $(function () {

          $.support.transition = (function () {

            var transitionEnd = (function () {

              var el = document.createElement('bootstrap')
                , transEndEventNames = {
                     'WebkitTransition' : 'webkitTransitionEnd'
                  ,  'MozTransition'    : 'transitionend'
                  ,  'OTransition'      : 'oTransitionEnd otransitionend'
                  ,  'transition'       : 'transitionend'
                  }
                , name

              for (name in transEndEventNames){
                if (el.style[name] !== undefined) {
                  return transEndEventNames[name]
                }
              }

            }())

            return transitionEnd && {
              end: transitionEnd
            }

          })()

        })

      }(window.jQuery);
    </script>
    <script type="text/javascript">
      /* ==========================================================
       * bootstrap-alert.js v2.3.2
       * http://twbs.github.com/bootstrap/javascript.html#alerts
       * ==========================================================
       * Copyright 2013 Twitter, Inc.
       *
       * Licensed under the Apache License, Version 2.0 (the "License");
       * you may not use this file except in compliance with the License.
       * You may obtain a copy of the License at
       *
       * http://www.apache.org/licenses/LICENSE-2.0
       *
       * Unless required by applicable law or agreed to in writing, software
       * distributed under the License is distributed on an "AS IS" BASIS,
       * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       * See the License for the specific language governing permissions and
       * limitations under the License.
       * ========================================================== */


      !function ($) {

        "use strict"; // jshint ;_;


       /* ALERT CLASS DEFINITION
        * ====================== */

        var dismiss = '[data-dismiss="alert"]'
          , Alert = function (el) {
              $(el).on('click', dismiss, this.close)
            }

        Alert.prototype.close = function (e) {
          var $this = $(this)
            , selector = $this.attr('data-target')
            , $parent

          if (!selector) {
            selector = $this.attr('href')
            selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') //strip for ie7
          }

          $parent = $(selector)

          e && e.preventDefault()

          $parent.length || ($parent = $this.hasClass('alert') ? $this : $this.parent())

          $parent.trigger(e = $.Event('close'))

          if (e.isDefaultPrevented()) return

          $parent.removeClass('in')

          function removeElement() {
            $parent
              .trigger('closed')
              .remove()
          }

          $.support.transition && $parent.hasClass('fade') ?
            $parent.on($.support.transition.end, removeElement) :
            removeElement()
        }


       /* ALERT PLUGIN DEFINITION
        * ======================= */

        var old = $.fn.alert

        $.fn.alert = function (option) {
          return this.each(function () {
            var $this = $(this)
              , data = $this.data('alert')
            if (!data) $this.data('alert', (data = new Alert(this)))
            if (typeof option == 'string') data[option].call($this)
          })
        }

        $.fn.alert.Constructor = Alert


       /* ALERT NO CONFLICT
        * ================= */

        $.fn.alert.noConflict = function () {
          $.fn.alert = old
          return this
        }


       /* ALERT DATA-API
        * ============== */

        $(document).on('click.alert.data-api', dismiss, Alert.prototype.close)

      }(window.jQuery);
    </script>
    <script type="text/javascript">
      /**
       * bootstrap-notify.js v1.0
       * --
       * http://twitter.com/nijikokun
       * Copyright 2012 Nijiko Yonskai, Goodybag
       * --
       * Licensed under the Apache License, Version 2.0 (the "License");
       * you may not use this file except in compliance with the License.
       * You may obtain a copy of the License at
       *
       * http://www.apache.org/licenses/LICENSE-2.0
       *
       * Unless required by applicable law or agreed to in writing, software
       * distributed under the License is distributed on an "AS IS" BASIS,
       * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       * See the License for the specific language governing permissions and
       * limitations under the License.
       */

      !function ($) {
        var Notification = function (element, options) {
          var self = this

          // Element collection
          this.$element = $(element)
          this.$note    = $('<div class="alert"></div>')
          this.options  = $.extend({}, $.fn.notify.defaults, options)

          // Setup from options
          if(this.options.transition)
            if(this.options.transition == 'fade')
              this.$note.addClass('in').addClass(this.options.transition)
            else this.$note.addClass(this.options.transition)
          else this.$note.addClass('fade').addClass('in')

          if(this.options.type)
            this.$note.addClass('alert-' + this.options.type)
          else this.$note.addClass('alert-success')

          if(typeof this.options.message === 'object')
            if(this.options.message.html)
              this.$note.html(this.options.message.html)
            else if(this.options.message.text)
              this.$note.text(this.options.message.text)
          else 
            this.$note.html(this.options.message)

          if(this.options.closable)
            this.$note.prepend($('<a class="close pull-right" data-dismiss="alert" href="#">&times;</a>'))

          return this;
        }

        Notification.prototype.show = function () {
          var self = this;

          if(this.options.fadeOut.enabled)
            this.$note.delay(this.options.fadeOut.delay || 3000).fadeOut('slow', function () {
              self.options.onClose()
              $(this).remove()
              self.options.onClosed()
            })

          this.$element.append(this.$note)
          this.$note.alert()
        }

        Notification.prototype.hide = function () {
          var self = this;

          if(this.options.fadeOut.enabled)
            this.$note.delay(this.options.fadeOut.delay || 3000).fadeOut('slow', function () {
              self.options.onClose()
              $(this).remove()
              self.options.onClosed()
            })
          else {
            self.options.onClose()
            this.$note.remove()
            self.options.onClosed()
          }
        }

        $.fn.notify = function (options) {
          return new Notification(this, options)
        }

        $.fn.notify.defaults = {
          type: 'success',
          closable: true,
          transition: 'fade',
          fadeOut: {
            enabled: true,
            delay: 3000
          },
          message: {
            html: false,
            text: 'This is a message.'
          },
          onClose: function () {},
          onClosed: function () {}
        }
      }(window.jQuery);
    </script>
    <script>
      // Random Messages
      var messages = [
        [ 'bottom-right', 'info',     'Gah this is awesome.'],
        [ 'top-right',    'success',  'I love Nijiko, he is my creator.' ],
        [ 'bottom-left',  'warning',  'Soda is bad.' ],
        [ 'top-right',  'danger',   "I'm sorry dave, I'm afraid I can't let you do that." ],
        [ 'bottom-right', 'info',     "There are only three rules." ],
        [ 'top-right',    'inverse',  'Do you hear me now?' ],
        [ 'bottom-left',  'blackgloss',     'You should fork this!' ]
      ];

      // Pretty print
      window.prettyPrint && prettyPrint()

      // Basic Features, style isn't even required.
      $('.show-notification').click(function (e) {
        var message = messages[Math.floor(Math.random() * messages.length)];
        
        $('.' + message[0]).notify({
          message: { text: message[2] },
          type: message[1],
          fadeOut: {
            delay: Math.floor(Math.random() * 500) + 2500
          }
        }).show();
      });
      
      /* Custom Styles */
      var custom = [
        'bangTidy',
        'blackgloss'
      ];
      
      for(var i = 0; i < custom.length; i++) {
        var type = custom[i];
        
        (function(type) {
          $('.show-' + type).click(function (e) {
            var message = messages[Math.floor(Math.random() * messages.length)];
            $('.' + message[0]).notify({ message: { text: message[2] }, type: type }).show();
          });
        })(type);
      }
    </script>
  </body>
</html>
